#!/bin/sh
DIR="$HOME/rpmbuild/BUILD/pinephone-pro-megieg25m-kernel-5.17/"
cp pinephone-pro-megieg25m-kernel-config $DIR/linux/.config
pushd $DIR/linux
make oldconfig
make menuconfig
make oldconfig
popd
cp $DIR/linux/.config pinephone-pro-megieg25m-kernel-config
