Name:           pinephone-pro-megieg25m-kernel
Version:        5.17
Release:        0.20220307.1220.1%{?dist}
Summary:        latest megi kernel for the pinephone pro

License:        GPLv3+
URL:            https://megous.com/git/linux/log/?h=orange-pi-5.17
Source0:        https://github.com/megous/linux/archive/refs/tags/orange-pi-5.17-20220307-1220.tar.gz
Source1:        megi-linux-firmware-amended.tar.gz
Source2:        pinephone-pro-megieg25m-kernel-config

Source3:        pinephone-pro-megieg25m-kernel-boot.cmd

Patch1:         0001-arm64-dts-rk3399-pinephone-pro-Remove-modem-node.patch
Patch2:         0002-arm64-dts-rk3399-pinephone-pro-add-modem-RI-pin.patch
Patch3:         0003-dts-pinephone-drop-modem-power-node.patch

BuildRequires:  uboot-tools
BuildRequires:  flex
BuildRequires:  bison
BuildRequires:  bc
BuildRequires:  openssl-devel
BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  perl

%description
Kernel RPMs built from megi's source with a couple of changes from a regular megi kernel:

- .config: disabled CONFIG_MODEM_POWER
- remove modem nodes
- add ring-indicator to facilitate wake by call

This package and eg25-manager go hand in hand. If you use a different kernel, eg25-manager
won't work (unless it's specifically labelled as such). If you use this kernel, your scripts
using modem-power won't work.

It is recommended to use Tow-Boot installed in your SPI with this kernel.

What doesn't work with this kernel as of now:

- cameras
- pinephone keyboard

This is a stopgap measure before UEFI kernels become a standard for PinePhones.

%global debug_package %{nil}

%prep
%setup -n %{name}-%{version} -T -c
tar -xf %{SOURCE0}
mv linux-orange-pi-* linux
tar -xf %{SOURCE1}

pushd linux
%patch1 -p1
%patch2 -p1
%patch3 -p1
popd

mkdir -p build
cp %{SOURCE2} build/.config
sed -i 's/CONFIG_LOCALVERSION=""/CONFIG_LOCALVERSION="-%{release}"/' build/.config
sed -i 's|CONFIG_EXTRA_FIRMWARE_DIR=.*|CONFIG_EXTRA_FIRMWARE_DIR="../linux-firmware"|' build/.config

mkdir -p uboot
cp %{SOURCE3} uboot/boot.cmd

%build
pushd uboot
mkimage -A arm -T script -O linux -d boot.cmd boot.scr
popd

# Pinephone Pro
CONFIG=pinephone_pro_defconfig
DTB=rockchip/rk3399-pinephone-pro.dtb

SRC=linux

CWD=`pwd`

export KBUILD_OUTPUT="$CWD/build"

mkdir -p "$KBUILD_OUTPUT" "$CWD/out"
test -f "$KBUILD_OUTPUT/.config" || make -C "$SRC" "$CONFIG"
make -C "$SRC" -j32 Image dtbs modules
make -C "$SRC" modules_install INSTALL_MOD_PATH="$CWD/out/modules"

%install
DTB=rockchip/rk3399-pinephone-pro.dtb

mkdir -p $RPM_BUILD_ROOT/boot/
cp uboot/boot.cmd uboot/boot.scr $RPM_BUILD_ROOT/boot/

mkdir -p $RPM_BUILD_ROOT/boot/rockchip/
mkdir -p $RPM_BUILD_ROOT/usr/
cp -a build/arch/arm64/boot/Image     $RPM_BUILD_ROOT/boot
cp -a build/arch/arm64/boot/dts/$DTB  $RPM_BUILD_ROOT/boot/rockchip/board.dtb
cp -a build/arch/arm64/boot/dts/$DTB  $RPM_BUILD_ROOT/boot/rockchip/rk3399-pinephone-pro.dtb
cp -a out/modules/*                   $RPM_BUILD_ROOT/usr/

%files
/boot/boot.cmd
/boot/boot.scr
/boot/Image
/boot/rockchip/
/usr/lib/modules/*

%changelog
* Mon Mar 07 2022 marcin - 5.17-0.20220307.1220.1
- Update megi kernel

* Mon Mar 07 2022 marcin - 5.17-0.20220223.0235.9
- Next attempt

* Mon Mar 07 2022 marcin - 5.17-0.20220223.0235.8
- Attempt to support LXC in order to support Waydroid

* Sun Mar 06 2022 marcin - 5.17-0.20220223.0235.7
- Fix build dependencies

* Sun Mar 06 2022 marcin - 5.17-0.20220223.0235.6
- Fix build dependencies

* Sun Mar 06 2022 marcin - 5.17-0.20220223.0235.5
- Enable CONFIG_ASHMEM

* Sun Mar 06 2022 marcin - 5.17-0.20220223.0235.4
- Clean up and finalize the spec

* Sun Mar 06 2022 marcin - 5.17-0.20220223.0235.3
- Adjust path to megi's linux-firmware

* Sun Mar 06 2022 marcin - 5.17-0.20220223.0235.2
- Rebuild

* Sun Mar 06 2022 marcin - 5.17-0.20220223.0235.1
- Fork into pinephone-pro-megieg25m-kernel

* Sat Feb 05 2022 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.17-1
- Update to 5.17

* Tue Jan 11 2022 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.16-1
- Update to 5.15.6

* Mon Dec 13 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.15.6-1
- Update to 5.15.6

* Wed Oct 13 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.14.12-1
- Update to 5.14.12

* Tue Sep 28 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.14.8-1
- Update to 5.14.8

* Wed Sep 22 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.14.6-1
- Update to 5.14.6

* Fri Sep 10 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.14.1-1
- Update to 5.14.1

* Fri Sep 03 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.14-1
- Update to 5.14

* Sat Aug 21 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.13.12-1
- Update to 5.13.12

* Mon Jul 26 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.13.4-1
- Update to 5.13.4

* Mon Jul 12 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.13.1-1
- Update to 5.13

* Sat Jun 12 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.10-1
- update 

* Sun May 23 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.6-1
- Update

* Thu May 06 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-7
- udpate 

* Wed Apr 07 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-6
- Update 

* Wed Apr 07 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-5
- Update for pp-uboot dep

* Fri Apr 02 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-4
- Update to latest 5.12

* Mon Mar 22 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-3
- Update to latest 5.12

* Mon Mar 15 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-2
- Update 5.12 kernel rc

* Thu Mar 04 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-1
- Update to 5.12 kernel

* Fri Feb 26 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.11.0-5
- Update to latest 

* Mon Feb 15 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.11.0-4
- Update to latest 

* Sun Feb 14 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.11.0-3
- Update to latest

* Sun Feb 07 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.11.0-2
- Update to latest

* Sun Jan 31 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.11.0-1
- Update to latest

* Fri Jan 22 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.9-1
- Update to latest

* Tue Jan 12 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.4-2
- Don't require device specific packages (phone vs tablet)

* Fri Jan 01 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.4-1
- Upgrade to 5.10.4

* Thu Dec 03 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.rc6b-4
- Upgrade to 5.10-rc6b

* Tue Dec 01 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.rc6-3
- Upgrade to 5.10-rc6

* Sat Oct 31 2020 Nikhil Jha <hi@nikhiljha.com> - 5.10-2
- Upgrade to 5.10 as of oct31

* Fri Oct 16 2020 Yoda - 5.9.0 GA
- Upgrade to 5.9.0 GA
- New audio driver - requires new pineaudio util
- New kernel layout - requires new uboot

* Fri Aug 28 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.9-2
- Upgrade to 5.9 rc2

* Fri Aug 21 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.9-1
- Upgrade to 5.9

* Thu Aug 06 2020 Nikhil Jha <hi@nikhiljha.com> - 5.8-1
- Upgrade to 5.8

* Mon Jun 15 2020 Nikhil Jha <hi@nikhiljha.com> - 5.7-1
- Initial packaging (hack)
