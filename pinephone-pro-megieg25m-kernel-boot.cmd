# Fedora megi-kernel loader script for PPP

if test ${mmc_bootdev} -eq 0 ; then
	echo "Booting from SD";
	setenv bootdev 1;
else
	echo "Booting from eMMC";
	setenv bootdev 2;
fi;

setenv bootargs console=ttyS2,115200n8 console=tty1 root=/dev/mmcblk${bootdev}p3 rootfstype=btrfs rw rootwait panic=5 loglevel=15

# printenv

gpio set 105
gpio set 154


echo Loading DTB
load mmc ${devnum}:2 ${fdt_addr_r} ${fdtfile}

echo Loading Kernel
load mmc ${devnum}:2 ${kernel_addr_r} Image
gpio clear 105

echo Resizing FDT
fdt addr ${fdt_addr_r}
fdt resize

echo Booting kernel
booti ${kernel_addr_r} - ${fdt_addr_r}
