# pinephone-pro-megieg25m-kernel

Kernel RPMs built from megi's source with a couple of changes from a regular megi kernel:

- .config: disabled CONFIG_MODEM_POWER
- remove modem nodes
- add ring-indicator to facilitate wake by call

This package and eg25-manager go hand in hand. If you use a different kernel, eg25-manager
won't work (unless it's specifically labelled as such). If you use this kernel, your scripts
using modem-power won't work.

It is recommended to use Tow-Boot installed in your SPI with this kernel.

What doesn't work with this kernel as of now:

- cameras
- pinephone keyboard

